/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinica;

import java.util.Date;

/**
 *
 * @author root
 */
public class PerAdministrativo extends Persona{
    
    //Atributos del Personal Administrativo
    private int salario;
    private String puesto;
    //private String Horario 

    public PerAdministrativo(){
    }

    public PerAdministrativo(int salario, String puesto, String cedula, String nombre, int edad, String email, String fechaNacimiento) {
        super(cedula, nombre, edad, email, fechaNacimiento);
        this.salario = salario;
        this.puesto = puesto;
    }
    
    

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "PerAdministrativo{" + "salario=" + salario + ", puesto=" + puesto + '}';
    }
    
    
    
}
