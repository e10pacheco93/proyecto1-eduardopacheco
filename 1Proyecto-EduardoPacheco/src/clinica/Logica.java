/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinica;

/**
 *
 * @author root
 */
public class Logica {
    
    private final Doctor[] doctores;
    private final Paciente[] pacientes;
    private final PerAdministrativo[] personal;
    
    
    public Logica(){
        doctores = new Doctor[15];
        pacientes = new Paciente[15];
        personal = new PerAdministrativo[10];
    }

    public Logica(Doctor[] doctores, Paciente[] pacientes, PerAdministrativo[] personal) {
        this.doctores = doctores;
        this.pacientes = pacientes;
        this.personal = personal;
    }
    
    public boolean registrarD(Doctor doc){
        
        for (int i = 0; i < doctores.length; i++) {
            if (doctores[i] == null) {
                doctores[i] = doc;
                return true;
            }
        }
        return false;
    }
    
    public boolean registrarP(Paciente pac){
        
        for (int i = 0; i < pacientes.length; i++) {
            if (pacientes[i] == null) {
                pacientes[i] = pac;
                return true;
            }
        }
        return false;
    }
    
    public boolean registrarA(PerAdministrativo pa){
        
        for (int i = 0; i < personal.length; i++) {
            if (personal[i] == null) {
                personal[i] = pa;
                return true;
            }
        }
        return false;
    }
    
    
    
    
    
    
    public String getDoctores(){
        String txt = "Lista de Doctores n";
        int num = 1;
        for (Doctor doctore : doctores) {
            if (doctore != null) {
                txt += num + ". " + doctore.getNombre() + "\n";
            }else{
                 txt += num + ". _____________________\n";
            }
            num++;
        }
        return txt;
    }
    
    
    
    public Doctor existeD(String ced){
        
           for (Doctor doctore : doctores) {
               if (doctore != null && doctore.getCedula().equals(ced)) {
                   return doctore;
               }
        }
        return null;
    }
    
    public Paciente existeP(String ced){
        
           for (Paciente paciente : pacientes) {
               if (paciente != null && paciente.getCedula().equals(ced)) {
                   return paciente;
               }
        }
        return null;
    }
    
    public PerAdministrativo existeA(String ced){
        
        for (PerAdministrativo perAdm : personal) {
            if (perAdm != null && perAdm.getCedula().equals(ced)) {
                return perAdm;
            }
        }    
        return null;
    }
}
