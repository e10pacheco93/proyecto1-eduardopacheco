/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinica;

/**
 *
 * @author root
 */
public class Paciente extends Persona{
    
    //Atributos del Paciente
    private String doctorAsig;
    private int estadoFinan;
    private int celular;
    private String proximaCita;

    public Paciente(){
    }

    public Paciente(String doctorAsig, int estadoFinan, int celular, String proximaCita, String cedula, String nombre, int edad, String email, String fechaNacimiento) {
        super(cedula, nombre, edad, email, fechaNacimiento);
        this.doctorAsig = doctorAsig;
        this.estadoFinan = estadoFinan;
        this.celular = celular;
        this.proximaCita = proximaCita;
    }
    
    

    public String getDoctorAsig() {
        return doctorAsig;
    }

    public void setDoctorAsig(String doctorAsig) {
        this.doctorAsig = doctorAsig;
    }

    public int getEstadoFinan() {
        return estadoFinan;
    }

    public void setEstadoFinan(int estadoFinan) {
        this.estadoFinan = estadoFinan;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public String getProximaCita() {
        return proximaCita;
    }

    public void setProximaCita(String proximaCita) {
        this.proximaCita = proximaCita;
    }

    @Override
    public String toString() {
        return "Paciente{" + "doctorAsig=" + doctorAsig + ", estadoFinan=" + estadoFinan + ", celular=" + celular + ", proximaCita=" + proximaCita + '}';
    }
    
    
    
}
