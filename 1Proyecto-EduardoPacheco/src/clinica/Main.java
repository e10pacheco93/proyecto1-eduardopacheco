package clinica;

import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
             
        
        //Inicializo la logica
        Logica log = new Logica();
        
        // Menu Inicial
        String menuI = "1. Ingresar al Sistema\n"
                + "2. Registrarse al Sistema\n"
                + "3. Salir\n";
        
        // Menu Registar 
        String[] menuR = {"1. Doctor", "2. Paciente", "3. Personal Administrativo", "Salir "};
        
        
        String[] menuDD = {"1. Buscar", "2. Listar", "3. Salir"};
        
             APP:
        while (true) {
            int op = Util.leerInt(menuI); 
           switch (op) {
                case 1:
                    BA:
                    while (true) {                        
                        op = Util.menuOpciones(menuR, "Seleccione una opción:") + 1;
                        switch (op){
                            case 1:
                                String ced = Util.leer("Cédula: ");
                                Persona cli = log.existeD(ced);
                                if (cli == null){
                                    if (Util.confirmar("Desea Registrarse? ")) {
                                        String titulo = Util.leer("Titulos Universitarios: (Dividalos entre comas.)");
                                        int salario = Util.leerInt("Salario: ");
                                        String especialidad = Util.leer("Especialidad: ");
                                        String estado = Util.leer("Estado: ");

                                        String cedula = Util.leer("Cedula: ");                         
                                        String nom = Util.leer("Nombre:");
                                        int edad = Util.leerInt("Edad: ");
                                        String email = Util.leer("Correo Electronico: ");
                                        String fechaNacimiento = Util.leer("Fecha de Nacimiento:(DD/MM/AAAA) ");
                                        
                                        Doctor docn = new Doctor(salario, titulo, especialidad, estado, cedula, nom, edad, email, fechaNacimiento);
                                        if (!log.registrarD(docn)) {
                                            Util.error("No se puede registrar... ");
                                            break;
                                        }
                                        
                                    }
                                    
                                    
                                }else{
                                    // Metodo del doctor
                                        break;
                                    }
                                break;
                            case 2:
                                 ced = Util.leer("Cédula: ");
                                 cli = log.existeP(ced);
                                 if (cli == null) {
                                     if (Util.confirmar("Desea Registrarse? ")) {
                                        int estadoFinan = Util.leerInt("Estado Financiero: ");
                                        int celular = Util.leerInt("Numero Celular: ");

                                        String cedula = Util.leer("Cedula: ");                         
                                        String nom = Util.leer("Nombre:");
                                        int edad = Util.leerInt("Edad: ");
                                        String email = Util.leer("Correo Electronico: ");
                                        String fechaNacimiento = Util.leer("Fecha de Nacimiento:(DD/MM/AAAA) ");
                                        
                                        Paciente paNew = new Paciente(null, estadoFinan, celular, null, cedula, nom, edad, email, fechaNacimiento);
                                        if (!log.registrarP(paNew)) {
                                            Util.error("No se puede registrar... ");
                                            break; 
                                         }
                                     }
                                     }else{
                                        // Metodo del paciente
                                         JOptionPane.showMessageDialog(null, "Bienvenido al Sistema...");
                                         break;
                                         
                                        
                                }
                                 
                                break;
                            case 3:
                                ced = Util.leer("Cedula: ");
                                cli = log.existeA(ced);
                                if (cli == null) {
                                    if (Util.confirmar("Desea Registrarse? ")) {
                                        
                                        
                                        int salario = Util.leerInt("Salario: ");
                                        String puesto = Util.leer("Puesto: ");
                                        String cedula = Util.leer("Cedula: ");                         
                                        String nom = Util.leer("Nombre:");
                                        int edad = Util.leerInt("Edad: ");
                                        String email = Util.leer("Correo Electronico: ");
                                        String fechaNacimiento = Util.leer("Fecha de Nacimiento:(DD/MM/AAAA) ");

                                        PerAdministrativo perAd = new PerAdministrativo(salario, puesto, cedula, nom, edad, email, fechaNacimiento);
                                        if (!log.registrarA(perAd)) {
                                            Util.error("No se puede registrar... ");
                                            break;
                                        }
                                    }
                                }else{
                                    //Metodo de Personal Administrador
                                        JOptionPane.showMessageDialog(null, "Bienvenido al Sistema...");
                                         break;
                                }
                                
                                break;
                            case 4:
                                break BA;
                        }
                    }
                    break;
                case 2:
                    BB:
                    while (true) {                        
                        op = Util.menuOpciones(menuR, "Seleccione una opción:") + 1;
                        switch (op) {
                            //Case Doctores
                        case 1:
                            JOptionPane.showMessageDialog(null, "Desea registrar Doctor...");
                            
                            String titulo = Util.leer("Titulos Universitarios: (Dividalos entre comas.)");
                            int salario = Util.leerInt("Salario: ");
                            String especialidad = Util.leer("Especialidad: ");
                            String estado = Util.leer("Estado: ");
                            
                            String cedula = Util.leer("Cedula: ");                         
                            String nom = Util.leer("Nombre:");
                            int edad = Util.leerInt("Edad: ");
                            String email = Util.leer("Correo Electronico: ");
                            String fechaNacimiento = Util.leer("Fecha de Nacimiento:(DD/MM/AAAA) ");
                            
                            Doctor docNew = new Doctor(salario, titulo, especialidad, estado, cedula, nom, edad, email, fechaNacimiento);
                            log.registrarD(docNew);
                            break ;
                            //Case Pacientes
                        case 2:
                            JOptionPane.showMessageDialog(null, "Desea registrar un Paciente...");
                            
                            
                            int estadoFinan = Util.leerInt("Estado Financiero: ");
                            int celular = Util.leerInt("Numero Celular: ");
                            
                             cedula = Util.leer("Cedula: ");                         
                             nom = Util.leer("Nombre:");
                             edad = Util.leerInt("Edad: ");
                             email = Util.leer("Correo Electronico: ");
                             fechaNacimiento = Util.leer("Fecha de Nacimiento:(DD/MM/AAAA) ");
                            
                            Paciente pacNew = new Paciente(null, estadoFinan, celular, null, cedula, nom, edad, email, fechaNacimiento);
                            log.registrarP(pacNew);
                            break;
                        case 3:
                            JOptionPane.showMessageDialog(null, "Desea registrar Personal administrativo...");
                            
                            salario = Util.leerInt("Salario: ");
                            String puesto = Util.leer("Puesto: ");
                            cedula = Util.leer("Cedula: ");                         
                            nom = Util.leer("Nombre:");
                            edad = Util.leerInt("Edad: ");
                            email = Util.leer("Correo Electronico: ");
                            fechaNacimiento = Util.leer("Fecha de Nacimiento:(DD/MM/AAAA) ");
                            
                            PerAdministrativo perAd = new PerAdministrativo(salario, puesto, cedula, nom, edad, email, fechaNacimiento);
                            log.registrarA(perAd);
                            
                            break;
                        case 4:
                            break BB;
                        
                    }
                }
                    break;
                case 3:
                   break APP;
            }
        }
    }
    
}
