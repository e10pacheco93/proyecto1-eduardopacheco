
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinica;

public class Doctor extends Persona{

    //Atributos del Doctor 
    private int salario;
    private String titulo;
    private String especialidad;
    private String estado;
    

    public Doctor(){
    }


    public Doctor(int salario, String titulo, String especialidad, String estado, String cedula, String nombre, int edad, String email, String fechaNacimiento) {
        super(cedula, nombre, edad, email, fechaNacimiento);
        this.salario = salario;
        this.titulo = titulo;
        this.especialidad = especialidad;
        this.estado = estado;
    }

    

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Doctor{" + "salario=" + salario + ", titulo=" + titulo + ", especialidad=" + especialidad + ", estado=" + estado + '}';
    }

    
    
}
