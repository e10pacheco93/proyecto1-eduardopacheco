/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinica;

/**
 *
 * @author root
 */
public class Persona {
    
    //Atributos de la persona..
    private String cedula;
    private String nombre;
    private int edad;
    private String email;   
    private String fechaNacimiento;
    
    public Persona(){
    }
    
    public Persona(String cedula, String nombre, int edad, String email, String fechaNacimiento) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.edad = edad;
        this.email = email;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", edad=" + edad + ", email=" + email + ", fechaNacimiento=" + fechaNacimiento + '}';
    }
    
    
    
    

    
    
}
