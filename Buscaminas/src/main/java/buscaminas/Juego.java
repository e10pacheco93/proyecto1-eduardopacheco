/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author root
 */
public class Juego {

    public Juego() {
    }
    
    public String[][] llenarMatriz() {
        Random r = new Random();
        String valor, cadena;
        int resultado;
        Scanner sc = new Scanner(System.in);
        int nfilas, ncolumnas;
        System.out.print("Ingrese las filas : ");
        nfilas = sc.nextInt();
        System.out.print("Ingrese las columnas : ");
        ncolumnas = sc.nextInt();
        String matriz[][] = new String[nfilas][ncolumnas];
        for (int i = 0; i < nfilas; i++) {
            for (int j = 0; j < ncolumnas; j++) {
                cadena = "01";
                cadena.charAt(r.nextInt(cadena.length()));
                valor = String.valueOf(cadena.charAt(r.nextInt(cadena.length())));
                matriz[i][j] = valor;
            }
        }
        return matriz;
    }

    public void imprimirMatriz(String[][] matriz) {
        for (int x = 0; x < matriz.length; x++) {
            for (int y = 0; y < matriz[x].length; y++) {
                System.out.print(matriz[x][y] + "\t");
            }
            System.out.println("\n");
        }
    }

    public void iniciarJuego(String[][] matriz) {
        Scanner sc = new Scanner(System.in);
        int nfila, ncolumna;
        int vidas = 0, oportunidades, contador = 0, Aciertos = 0;
        boolean validaVida = true, seguir = true;
        int restantes = 0;
        System.out.println("Trampa esta viendo la matriz : ");

        imprimirMatriz(matriz);

        System.out.println("Inicia el juego \n ");

        oportunidades = matriz.length * matriz[0].length;

        System.out.println("Sus oportunidades son : " + oportunidades + "\n");
        System.out.print("Ingrese la cantidad de vidas deseadas : ");

        while (validaVida) {
            vidas = sc.nextInt();
            if (vidas <= 0) {
                System.out.println("Debe digitar al menos una vida");
            } else {
                validaVida = false;
            }
        }

        while (vidas > oportunidades) {

            System.out.println("Las vidas no pueden ser superiores a las oportunidades");
            System.out.println("Ingrese nuevamente las vidas");
            vidas = sc.nextInt();
        }

        do {
            try {
                System.out.print("Ingrese la fila donde desea jugar : ");
                nfila = sc.nextInt();
                System.out.print("Ingrese la columna donde desea jugar : ");
                ncolumna = sc.nextInt();

                if (Integer.parseInt(matriz[nfila][ncolumna]) == 1) {
                    contador += 1;
                    matriz[nfila][ncolumna] = "X";
                    restantes = vidas - contador;
                    System.out.println("Mina Exploto te quedan : " + restantes + " : vidas");
                } else {
                    Aciertos += 1;
                    matriz[nfila][ncolumna] = "@";
                    System.out.println("Espacio libre! Fila:" + nfila + " Columna: " + ncolumna  );
                }
                if (contador == vidas) {
                    seguir = false;
                    System.out.println("La matriz es : \n");
                    imprimirMatriz(matriz);
                    System.out.println("\n");
                    System.out.println("Alcanzo el liminte de vidas \n"
                            + "vidas: " + vidas
                            + " | Explociones : " + contador);
                }
                if (Aciertos == oportunidades) {
                    seguir = false;
                    System.out.println("La matriz es : \n");
                    imprimirMatriz(matriz);
                    System.out.println("\n");
                    System.out.println("Alcanzo el total de Aciertos \n"
                            + "vidas: " + vidas
                            + " | Explociones : " + contador 
                            + "| Acertados: "+ Aciertos);
                }
            } catch (Exception e) {
                System.out.println("Campo no existe o lugar ya jugado ");
            } finally {
                System.out.println("Muchas Gracias por jugar ");
            }
        } while (seguir);
    }
}
