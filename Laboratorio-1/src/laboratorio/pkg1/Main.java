/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.pkg1;

import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // Varibles 
        int capacidad = 0;
        int totalEspacio = 0;
        int precioxHora = 0;
        int total = 0;
        double totalFinal = 0;
        double totalHoras = 0;
        double descuento = 0;
        int horas = 0;
        boolean regresar = true;
        int salir;
        
        String factura;

        // Variables constantes 
        
        final int costoxhorasAutos = 700;
        final int costoxhorasCamiones = 1500;
        final int costoxhorasMotos = 250;
        int capacidadAutos = 25;
        int capacidadCamiones = 15;
        int capacidadMotos = 30;

        while (regresar) {
            // Menu del Parqueo
            Object[] menuVehiculos = {"Automoviles", "Motocicletas", "Camiones", "Salir"};
            String vehiculoInicial = "Automoviles";
            Object seleccionVehiculo = JOptionPane.showInputDialog(null, "Seleccione el Vehiculo a Ingresar", "Bienvenido al Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE, null, menuVehiculos, vehiculoInicial);
            String listVehiculos = seleccionVehiculo.toString();

            switch (listVehiculos) {

                case "Automoviles":
                    do {
                        try {
                            Object[] opciones = {"Frecuente", "No Frecuente"};
                            String eleccion = "Cliente Frecuente";
                            Object opcionFinal = JOptionPane.showInputDialog(null, "Seleccione el tipo de Cliente", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE, null, opciones, eleccion);
                            String tipoCliente = opcionFinal.toString();

                            if ("Frecuente".equals(tipoCliente)) {
                                capacidad = Integer.parseInt(JOptionPane.showInputDialog(null, "Capacidad para " + listVehiculos + " es de " + capacidadAutos + "\n Ingrese la cantidad a estacionar: ", "Sistema de Parquep", JOptionPane.QUESTION_MESSAGE));
                                totalEspacio = capacidadAutos - capacidad;

                                if (totalEspacio > 0) {
                                    horas = Integer.parseInt(JOptionPane.showInputDialog(null, " Digite las horas que va estar estacionado \n"
                                            + "", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE));
                                    precioxHora = horas * costoxhorasAutos;

                                    if (capacidad >= 5) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.30;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 3) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.15;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 1) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.05;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 2 || capacidad == 4) {
                                        totalFinal = precioxHora * capacidad;
                                    }
                                    if (horas > 72) {
                                        totalHoras = totalFinal * 0.02;
                                        totalFinal = totalFinal - totalHoras;
                                    }
                                    factura = "Factura Preforma\n"
                                            + "Tipo Vehiculo: " + listVehiculos + "\n"
                                            + "Cantidad de Vehiculos: " + capacidad + "\n"
                                            + "Cantidad de horas: " + horas + "\n"
                                            + "----------------------------------------\n"
                                            + "\n"
                                            + "Costo total a pagar: ₡ " + total + "\n"
                                            + "Costo del descuento: ₡ " + descuento + "\n"
                                            + "descuento de los 3 días: ₡ " + totalHoras + "\n"
                                            + "Costo Final a pagar : ₡ " + totalFinal + " \n";

                                    JOptionPane.showMessageDialog(null, factura, "Sistema de Parqueo", JOptionPane.INFORMATION_MESSAGE);
                                    break;

                                } else {
                                    JOptionPane.showMessageDialog(null, "Lo sentimos a excedido la capacidad de automoviles", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                                    break;
                                }
                            } else {
                                //Cliente no Frecuente
                                capacidad = Integer.parseInt(JOptionPane.showInputDialog(null, "Capacidad para " + listVehiculos + " es de " + capacidadAutos + "\n Ingrese la cantidad a estacionar: ", "Sistema de Parquep", JOptionPane.QUESTION_MESSAGE));
                                totalEspacio = capacidadAutos - capacidad;

                                if (totalEspacio > 0) {
                                    horas = Integer.parseInt(JOptionPane.showInputDialog(null, " Digite las horas que va estar estacionado \n"
                                            + "", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE));
                                    precioxHora = horas * costoxhorasAutos;
                                    total = precioxHora * capacidad;
                                    descuento = total * 0.02;
                                    totalFinal = total - descuento;
                                    
                                    if (horas > 72) {
                                        totalHoras = totalFinal * 0.02;
                                        totalFinal = totalFinal - totalHoras;
                                    }
                                    factura = "Factura Preforma\n"
                                            + "Tipo Vehiculo: " + listVehiculos + "\n"
                                            + "Cantidad de Vehiculos: " + capacidad + "\n"
                                            + "Cantidad de horas: " + horas + "\n"
                                            + "----------------------------------------\n"
                                            + "\n"
                                            + "Costo total a pagar: ₡ " + total + "\n"
                                            + "Costo del descuento: ₡ " + descuento + "\n"
                                            + "descuento de los 3 días: ₡ " + totalHoras + "\n"
                                            + "Costo Final a pagar : ₡ " + totalFinal + " \n";

                                    JOptionPane.showMessageDialog(null, factura, "Sistema de Parqueo", JOptionPane.INFORMATION_MESSAGE);
                                    break;

                                } else {
                                    JOptionPane.showMessageDialog(null, "Lo sentimos a excedido la capacidad de automoviles", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                                    break;

                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Opcíon invalida", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                            break;
                        }

                    } while (capacidadAutos > 0);
                    
                    break;
                    
                case "Motocicletas":
                    do {
                        try {
                            Object[] opciones = {"Frecuente", "No Frecuente"};
                            String eleccion = "Cliente Frecuente";
                            Object opcionFinal = JOptionPane.showInputDialog(null, "Seleccione el tipo de Cliente", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE, null, opciones, eleccion);
                            String tipoCliente = opcionFinal.toString();

                            if ("Frecuente".equals(tipoCliente)) {
                                capacidad = Integer.parseInt(JOptionPane.showInputDialog(null, "Capacidad para " + listVehiculos + " es de " + capacidadMotos + "\n Ingrese la cantidad a estacionar: ", "Sistema de Parquep", JOptionPane.QUESTION_MESSAGE));
                                totalEspacio = capacidadMotos - capacidad;

                                if (totalEspacio > 0) {
                                    horas = Integer.parseInt(JOptionPane.showInputDialog(null, " Digite las horas que va estar estacionado \n"
                                            + "", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE));
                                    precioxHora = horas * costoxhorasMotos;

                                    if (capacidad >= 5) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.30;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 3) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.15;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 1) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.05;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 2 || capacidad == 4) {
                                        totalFinal = precioxHora * capacidad;
                                    }
                                    if (horas > 72) {
                                        totalHoras = totalFinal * 0.02;
                                        totalFinal = totalFinal - totalHoras;
                                    }
                                    factura = "Factura Preforma\n"
                                            + "Tipo Vehiculo: " + listVehiculos + "\n"
                                            + "Cantidad de Vehiculos: " + capacidad + "\n"
                                            + "Cantidad de horas: " + horas + "\n"
                                            + "----------------------------------------\n"
                                            + "\n"
                                            + "Costo total a pagar: ₡ " + total + "\n"
                                            + "Costo del descuento: ₡ " + descuento + "\n"
                                            + "descuento de los 3 días: ₡ " + totalHoras + "\n"
                                            + "Costo Final a pagar : ₡ " + totalFinal + " \n";

                                    JOptionPane.showMessageDialog(null, factura, "Sistema de Parqueo", JOptionPane.INFORMATION_MESSAGE);
                                    break;

                                } else {
                                    JOptionPane.showMessageDialog(null, "Lo sentimos a excedido la capacidad de motocicletas", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                                    break;
                                }
                            } else {
                                //Cliente no Frecuente
                                capacidad = Integer.parseInt(JOptionPane.showInputDialog(null, "Capacidad para " + listVehiculos + " es de " + capacidadMotos + "\n Ingrese la cantidad a estacionar: ", "Sistema de Parquep", JOptionPane.QUESTION_MESSAGE));
                                totalEspacio = capacidadMotos - capacidad;

                                if (totalEspacio > 0) {
                                    horas = Integer.parseInt(JOptionPane.showInputDialog(null, " Digite las horas que va estar estacionado \n"
                                            + "", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE));
                                    precioxHora = horas * costoxhorasMotos;
                                    total = precioxHora * capacidad;
                                    descuento = total * 0.02;
                                    totalFinal = total - descuento;
                                    
                                    if (horas > 72) {
                                        totalHoras = totalFinal * 0.02;
                                        totalFinal = totalFinal - totalHoras;
                                    }
                                    factura = "Factura Preforma\n"
                                            + "Tipo Vehiculo: " + listVehiculos + "\n"
                                            + "Cantidad de Vehiculos: " + capacidad + "\n"
                                            + "Cantidad de horas: " + horas + "\n"
                                            + "----------------------------------------\n"
                                            + "\n"
                                            + "Costo total a pagar: ₡ " + total + "\n"
                                            + "Costo del descuento: ₡ " + descuento + "\n"
                                            + "descuento de los 3 días: ₡ " + totalHoras + "\n"
                                            + "Costo Final a pagar : ₡ " + totalFinal + " \n";

                                    JOptionPane.showMessageDialog(null, factura, "Sistema de Parqueo", JOptionPane.INFORMATION_MESSAGE);
                                    break;

                                } else {
                                    JOptionPane.showMessageDialog(null, "Lo sentimos a excedido la capacidad de motocicletas", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                                    break;

                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Opcíon invalida", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                            break;
                        }

                    } while (capacidadAutos > 0);
                    break;
                case "Camiones": 
                    do {
                        try {
                            Object[] opciones = {"Frecuente", "No Frecuente"};
                            String eleccion = "Cliente Frecuente";
                            Object opcionFinal = JOptionPane.showInputDialog(null, "Seleccione el tipo de Cliente", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE, null, opciones, eleccion);
                            String tipoCliente = opcionFinal.toString();

                            if ("Frecuente".equals(tipoCliente)) {
                                capacidad = Integer.parseInt(JOptionPane.showInputDialog(null, "Capacidad para " + listVehiculos + " es de " + capacidadCamiones + "\n Ingrese la cantidad a estacionar: ", "Sistema de Parquep", JOptionPane.QUESTION_MESSAGE));
                                totalEspacio = capacidadCamiones - capacidad;

                                if (totalEspacio > 0) {
                                    horas = Integer.parseInt(JOptionPane.showInputDialog(null, " Digite las horas que va estar estacionado \n"
                                            + "", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE));
                                    precioxHora = horas * costoxhorasCamiones;

                                    if (capacidad >= 5) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.30;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 3) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.15;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 1) {

                                        total = precioxHora * capacidad;
                                        descuento = total * 0.05;
                                        totalFinal = total - descuento;
                                        

                                    } else if (capacidad == 2 || capacidad == 4) {
                                        totalFinal = precioxHora * capacidad;
                                    }
                                    if (horas > 72) {
                                        totalHoras = totalFinal * 0.02;
                                        totalFinal = totalFinal - totalHoras;
                                    }
                                    factura = "Factura Preforma\n"
                                            + "Tipo Vehiculo: " + listVehiculos + "\n"
                                            + "Cantidad de Vehiculos: " + capacidad + "\n"
                                            + "Cantidad de horas: " + horas + "\n"
                                            + "----------------------------------------\n"
                                            + "\n"
                                            + "Costo total a pagar: ₡ " + total + "\n"
                                            + "Costo del descuento: ₡ " + descuento + "\n"
                                            + "descuento de los 3 días: ₡ " + totalHoras + "\n"
                                            + "Costo Final a pagar : ₡ " + totalFinal + " \n";

                                    JOptionPane.showMessageDialog(null, factura, "Sistema de Parqueo", JOptionPane.INFORMATION_MESSAGE);
                                    break;

                                } else {
                                    JOptionPane.showMessageDialog(null, "Lo sentimos a excedido la capacidad de camiones", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                                    break;
                                }
                            } else {
                                //Cliente no Frecuente
                                capacidad = Integer.parseInt(JOptionPane.showInputDialog(null, "Capacidad para " + listVehiculos + " es de " + capacidadCamiones + "\n Ingrese la cantidad a estacionar: ", "Sistema de Parquep", JOptionPane.QUESTION_MESSAGE));
                                totalEspacio = capacidadCamiones - capacidad;

                                if (totalEspacio > 0) {
                                    horas = Integer.parseInt(JOptionPane.showInputDialog(null, " Digite las horas que va estar estacionado \n"
                                            + "", "Sistema de Parqueo", JOptionPane.QUESTION_MESSAGE));
                                    precioxHora = horas * costoxhorasCamiones;
                                    total = precioxHora * capacidad;
                                    descuento = total * 0.02;
                                    totalFinal = total - descuento;
                                    
                                    if (horas > 72) {
                                        totalHoras = totalFinal * 0.02;
                                        totalFinal = totalFinal - totalHoras;
                                    }
                                    factura = "Factura Preforma\n"
                                            + "Tipo Vehiculo: " + listVehiculos + "\n"
                                            + "Cantidad de Vehiculos: " + capacidad + "\n"
                                            + "Cantidad de horas: " + horas + "\n"
                                            + "----------------------------------------\n"
                                            + "\n"
                                            + "Costo total a pagar: ₡ " + total + "\n"
                                            + "Costo del descuento: ₡ " + descuento + "\n"
                                            + "descuento de los 3 días: ₡ " + totalHoras + "\n"
                                            + "Costo Final a pagar : ₡ " + totalFinal + " \n";

                                    JOptionPane.showMessageDialog(null, factura, "Sistema de Parqueo", JOptionPane.INFORMATION_MESSAGE);
                                    break;

                                } else {
                                    JOptionPane.showMessageDialog(null, "Lo sentimos a excedido la capacidad de camiones", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                                    break;

                                }

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Opcíon Invalida", "Sistema de Parqueo", JOptionPane.ERROR_MESSAGE);
                            break;
                        }

                    } while (capacidadCamiones > 0);
                    break;
                    
                default:
                    break;
                    

            }
            salir=JOptionPane.showConfirmDialog(null,"Seguro que desea salir?");
            if(salir==0){
            regresar=false;
            }
            

        }
    }

}
